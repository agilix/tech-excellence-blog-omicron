from django.db import models


class Post(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=100, blank=True, default='')
    author = models.CharField(max_length=100, blank=True, default='')
    text = models.CharField(max_length=5000, blank=True, default='')

    class Meta:
        ordering = ['created']
