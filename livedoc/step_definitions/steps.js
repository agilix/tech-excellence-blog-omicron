const {I} = inject();

let state = {};

When('я открываю главную страницу', () => {
    I.amOnPage('/')
})

Then('я вижу статус сервиса {string} в подвале', (status) => {
    I.see(`status: ${status}`)
})