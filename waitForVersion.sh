RETRIES=20
echo "Looking for version $VERSION on host $HOST"

URL="http://$HOST/version.txt"

while [ "$RETRIES" -gt "0" ] && [ "$(curl -s $URL)" != "$VERSION" ]; do
  sleep 5
  ((RETRIES=RETRIES-1))
  echo "Version $VERSION not found on $URL Retries left: #$RETRIES"
done

if [[ "$RETRIES" -eq 0 ]]; then
  echo "Version $VERSION not found on host $HOST"
  (exit 1)
fi

echo "Version found 😎"